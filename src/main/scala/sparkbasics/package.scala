/**
 * Package object for package sparkbasics.
 * The content of this object is accessible to everything inside this package
 */
package object sparkbasics {

  // Import spark based dependencies
  import org.apache.spark.SparkConf
  import org.apache.spark.SparkContext

  // Create spark configuration object with an app name and master URL
  val conf = new SparkConf().setAppName("MyApp").setMaster("spark://192.168.1.4:7077") // Notice the master URL is "local" for a simulation of local cluster
  // Create a spark context object
  val sc = new SparkContext(conf)

}

import scala.util.{Failure, Try, Success}

val myCollection: List[Any] = List("Hello", "alpha", 4.2, "beta", 10)

val tryBlock: List[Try[String]] = myCollection map {
  element => Try {
    element match {
      case e: Int => e.toString
      case e: String => e
      case _ => throw new Exception("Cannot match this element.")
      /* pattern matching inside the List, hence _*/
    }
  }
}

tryBlock foreach {
  v => v match {
    case Success(x: String) => println(x)
    case Failure(e) => e.printStackTrace()
}
}